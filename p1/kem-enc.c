/* kem-enc.c
 * simple encryption utility providing CCA2 security.
 * based on the KEM/DEM hybrid model. */

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <fcntl.h>
#include <openssl/sha.h>

#include "ske.h"
#include "rsa.h"
#include "prf.h"

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Encrypt or decrypt data.\n\n"
"   -i,--in     FILE   read input from FILE.\n"
"   -o,--out    FILE   write output to FILE.\n"
"   -k,--key    FILE   the key.\n"
"   -r,--rand   FILE   use FILE to seed RNG (defaults to /dev/urandom).\n"
"   -e,--enc           encrypt (this is the default action).\n"
"   -d,--dec           decrypt.\n"
"   -g,--gen    FILE   generate new key and write to FILE{,.pub}\n"
"   -b,--BITS   NBITS  length of new key (NOTE: this corresponds to the\n"
"                      RSA key; the symmetric key will always be 256 bits).\n"
"                      Defaults to %lu.\n"
"   --help             show this message and exit.\n";

#define FNLEN 255

enum modes {
	ENC,
	DEC,
	GEN
};

/* Let SK denote the symmetric key.  Then to format ciphertext, we
 * simply concatenate:
 * +------------+----------------+
 * | RSA-KEM(X) | SKE ciphertext |
 * +------------+----------------+
 * NOTE: reading such a file is only useful if you have the key,
 * and from the key you can infer the length of the RSA ciphertext.
 * We'll construct our KEM as KEM(X) := RSA(X)|H(X), and define the
 * key to be SK = KDF(X).  Naturally H and KDF need to be "orthogonal",
 * so we will use different hash functions:  H := SHA256, while
 * KDF := HMAC-SHA512, where the key to the hmac is defined in ske.c
 * (see KDF_KEY).
 * */

#define HASHLEN 32 /* for sha256 */

int kem_encrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	/* TODO: encapsulate random symmetric key (SK) using RSA and SHA256;
	 * encrypt fnIn with SK; concatenate encapsulation and cihpertext;
	 * write to fnOut. */
	  size_t _mLen = rsa_numBytesN(K);
	 // printf("%zu - ", _mLen);
	 unsigned char* pt = malloc(_mLen);
	 unsigned char* ct = malloc(_mLen);
	 randBytes(pt, _mLen-1);

	 size_t _ctLen; 
	 _ctLen = rsa_encrypt(ct, pt, _mLen, K);

	 unsigned char* HMACBuf;
	 HMACBuf = malloc(64);
	 HMAC(EVP_sha512(), KDF_KEY, 32, pt, _mLen, HMACBuf, NULL);

	 SKE_KEY _K;
	 
		 for (int i = 0; i < 32; i++)
			 _K.hmacKey[i] = pt[i];
	 
		 for (int i = 32; i < 64; i++)
			 _K.aesKey[i-32] = pt[i];
	
	
	int fd = open(fnIn, O_RDONLY);
	if (fd == -1) return -1;
	struct stat sb;
	if (fstat(fd, &sb) == -1) return -1;
	if (sb.st_size == 0) return -1;

	char* Text;
	Text = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	
	unsigned char* IV = malloc(16);
    for (int i = 0; i < 16; i++) IV[i] = i;
    size_t len = strlen(Text) + 1;
    size_t ctLen = ske_getOutputLen(len);
    unsigned char *SKE_ciphertext = malloc(ctLen+1);
	size_t total = ske_encrypt(SKE_ciphertext, (unsigned char*)Text, len, &_K, IV);
	
	unsigned char * final = malloc(_ctLen+64+total);
	memcpy(final, ct, _ctLen);
    memcpy(final+_ctLen, HMACBuf, 64);
	memcpy(final+_ctLen+64, SKE_ciphertext, total);
	
	int out_file = open(fnOut, O_CREAT | O_RDWR, S_IRWXU);
	write(out_file, final, (_ctLen+64+total));
	 
	return 0;
}

/* NOTE: make sure you check the decapsulation is valid before continuing */
int kem_decrypt(const char* fnOut, const char* fnIn, RSA_KEY* K)
{
	/* TODO: write this. */
	/* step 1: recover the symmetric key */
	/* step 2: check decapsulation */
	/* step 3: derive key from ephemKey and decrypt data. */
	int fd = open(fnIn, O_RDONLY);
    if (fd == -1) return -1;
    struct stat sb;
    if (fstat(fd, &sb) == -1) return -1;
	if (sb.st_size == 0) return -1;
	
	unsigned char* Text;
	Text = mmap(NULL, sb.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	
	size_t _mLen = rsa_numBytesN(K);
    unsigned char* RandEncryp = malloc(_mLen);
	memcpy(RandEncryp, Text, _mLen);
	
	unsigned char* RandDecryp = malloc(_mLen);
	rsa_decrypt(RandDecryp, RandEncryp, _mLen, K);
	
	unsigned char* HMACBuf;
	HMACBuf = malloc(64);
	HMAC(EVP_sha512(), KDF_KEY, 32, RandDecryp, _mLen, HMACBuf, NULL);

	unsigned char* symKey = malloc(64);
    memcpy(symKey, Text+_mLen, 64);

	for (int i = 0; i < 64; i++)
	{
		if (symKey[i] != HMACBuf[i])
		{
			printf("FAILEd\n");
			return -1;
		}
	}

   	SKE_KEY _K;

	for (int i = 0; i < 32; i++)
		_K.hmacKey[i] = RandDecryp[i];

	for (int i = 32; i < 64; i++)
		_K.aesKey[i-32] = RandDecryp[i];

	size_t SKE_cp_size = sb.st_size - 64 - _mLen;
	unsigned char* SKE_ciphertext = malloc(SKE_cp_size);
	memcpy(SKE_ciphertext, Text+_mLen+64, SKE_cp_size);
	
	unsigned char* pt = malloc(SKE_cp_size);
	size_t r = ske_decrypt(pt, SKE_ciphertext, SKE_cp_size, &_K);

	FILE *f = fopen(fnOut, "w");
    fprintf(f, "%s", pt);
    fclose(f);

	return 0;
}

int main(int argc, char *argv[]) {
	/* define long options */
	static struct option long_opts[] = {
		{"in",      required_argument, 0, 'i'},
		{"out",     required_argument, 0, 'o'},
		{"key",     required_argument, 0, 'k'},
		{"rand",    required_argument, 0, 'r'},
		{"gen",     required_argument, 0, 'g'},
		{"bits",    required_argument, 0, 'b'},
		{"enc",     no_argument,       0, 'e'},
		{"dec",     no_argument,       0, 'd'},
		{"help",    no_argument,       0, 'h'},
		{0,0,0,0}
	};
	/* process options: */
	char c;
	int opt_index = 0;
	char fnRnd[FNLEN+1] = "/dev/urandom";
	fnRnd[FNLEN] = 0;
	char fnIn[FNLEN+1];
	char fnOut[FNLEN+1];
	char fnKey[FNLEN+1];
	memset(fnIn,0,FNLEN+1);
	memset(fnOut,0,FNLEN+1);
	memset(fnKey,0,FNLEN+1);
	int mode = ENC;
	// size_t nBits = 2048;
	size_t nBits = 1024;
	while ((c = getopt_long(argc, argv, "edhi:o:k:r:g:b:", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'h':
				printf(usage,argv[0],nBits);
				return 0;
			case 'i':
				strncpy(fnIn,optarg,FNLEN);
				break;
			case 'o':
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'k':
				strncpy(fnKey,optarg,FNLEN);
				break;
			case 'r':
				strncpy(fnRnd,optarg,FNLEN);
				break;
			case 'e':
				mode = ENC;
				break;
			case 'd':
				mode = DEC;
				break;
			case 'g':
				mode = GEN;
				strncpy(fnOut,optarg,FNLEN);
				break;
			case 'b':
				nBits = atol(optarg);
				break;
			case '?':
				printf(usage,argv[0],nBits);
				return 1;
		}
	}

	/* TODO: finish this off.  Be sure to erase sensitive data
	 * like private keys when you're done with them (see the
	 * rsa_shredKey function). */
	switch (mode) {
		case ENC:
		case DEC:
		case GEN:
		default:
			return 1;
	}

	return 0;
}
